import { initDb } from '../db/postgre';
import { getRepository } from 'typeorm';
import { Log } from '../models/entities/log';

async function main() {
	await initDb();

	const Logs = getRepository(Log);

	await Logs.delete({});
}

main().then(() => {
	console.log('Success');
	process.exit(0);
}).catch(e => {
	console.error(`Error: ${e.message || e}`);
	process.exit(1);
});
